﻿![banner image](./CustomWeaponDamage/banner.png)

# Custom Weapon Damage
*([view on RaftModding](https://www.raftmodding.com/mods/customweapondamage))*

This mod lets you customize how much damage your weapons deal to animals, enemies and your friends.

## Changing the tool speed
Open the console by pressing the `[F10]` key and type the following command to change the weapon damage. The damage value is a multiplier to the damage of your weapons (i.e. a weapon damage value of 2 will double the amount of damage given by your weapons).

**Command: `weapondamage <damage>`**

**Example:** Typing `weapondamage 2` will double your weapons' damage.

## Multiplayer
In multiplayer, all players need to install this mod and set the weapon damage themselves. Each player can set the damage he deals automatically.

*A synchronization of the weapon damage between players might be added in a later release.*

## Support
If you have any issues with or ideas for this mod, please message me (@traxam#7012) in the modding section of the [official Raft Discord-server](https://discord.gg/raft).

## Credits
- developed by [traxam](https://trax.am/)
- [icon image](https://www.flaticon.com/free-icon/loyalty_1006128) by [Freepik](https://www.freepik.com/), licensed [Creative Commons BY 3.0](http://creativecommons.org/licenses/by/3.0/)
- Thanks to `Psaimon#3040` for the idea