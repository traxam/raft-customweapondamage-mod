﻿using System;
using System.Globalization;
using System.Reflection;
using HarmonyLib;
using Steamworks;
using UnityEngine;

namespace CustomWeaponDamage
{
    public class CustomWeaponDamageMod : Mod
    {
        private const string PlayerPrefsWeaponDamage = "traxam.CustomWeaponDamage.weaponDamage";
        private const string HarmonyId = "traxam.CustomWeaponDamage";
        private static float _currentWeaponDamage = 1;
        private Harmony _harmony;

        public void Start()
        {
            _harmony = new Harmony(HarmonyId);
            _harmony.PatchAll(Assembly.GetExecutingAssembly());

            Info("Mod was loaded successfully!");
            if (PlayerPrefs.HasKey(PlayerPrefsWeaponDamage))
            {
                _currentWeaponDamage = PlayerPrefs.GetFloat(PlayerPrefsWeaponDamage);
                FollowUpLog("The current weapon damage is <color=green>" + _currentWeaponDamage + "</color>.");
            }

            FollowUpLog(
                "Type <color=green>weapondamage <damage></color> to change the weapondamage, i.e. <color=green>weapondamage 2.5</color> (1 is the default weapon damage).");
        }

        [ConsoleCommand("weapondamage", "Change the weapon damage")]
        // ReSharper disable once UnusedMember.Local
        private static void HandleWeaponDamageCommand(string[] arguments)
        {
            if (arguments.Length < 1)
            {
                Info("The current weapon damage is <color=green>" + _currentWeaponDamage + "</color>.");
                FollowUpLog(
                    "Type <color=green>weapondamage <damage></color> to change the weapon damage, i.e. <color=green>weapondamage 2.5</color> (1 is the default weapon damage).");
            }
            else
            {
                try
                {
                    var value = float.Parse(arguments[0], CultureInfo.InvariantCulture);
                    if (value <= 0)
                    {
                        Error("The provided weapon damage (<color=green>" + arguments[0] +
                              "</color>) is not a positive value.");
                        FollowUpLog(
                            "Please provide a positive decimal weapon damage value, i.e. <color=green>weapondamage 2.5</color> (1 is the default weapon damage).");
                    }
                    else
                    {
                        _currentWeaponDamage = value;
                        PlayerPrefs.SetFloat(PlayerPrefsWeaponDamage, value);
                        PlayerPrefs.Save();
                        Info("Weapon damage was set to <color=green>" + value +
                             "</color>. Type <color=green>weapondamage 1</color> to reset it.");
                    }
                }
                catch (FormatException)
                {
                    Error("<color=green>" + arguments[0] +
                          "</color> is not a valid value. Please provide a positive decimal weapon damage value, i.e. <color=green>weapondamage 2.5</color> (1 is the default weapon damage).");
                }
            }
        }

        public void OnModUnload()
        {
            _harmony.UnpatchAll(HarmonyId);
            _currentWeaponDamage = 1;
            Info("Weapon damage was reset.");
        }

        private static void Info(string message)
        {
            Debug.Log("<color=#3498db>[info]</color>\t<b>traxam's Custom Weapon Damage:</b> " + message);
        }

        private static void FollowUpLog(string message)
        {
            Debug.Log("\t" + message);
        }

        private static void Error(string message)
        {
            Debug.LogError("<color=#e74c3c>[error]</color>\t<b>traxam's Custom Weapon Damage:</b> " + message);
        }

        [HarmonyPatch(typeof(Network_Host))]
        [HarmonyPatch("DamageEntity")]
        // This method is copied from the original Raft source code and was slightly modified to allow damage value
        // modification. To change as few things as possible, we are not considering ReSharper's code warnings.
        // ReSharper disable All
        private class DamageEntityPatch
        {
            private static bool Prefix(Network_Entity entity,
                Transform hitTransform,
                float damage,
                Vector3 hitPoint,
                Vector3 hitNormal,
                EntityType damageInflictorEntityType,
                SO_Buff buffAsset,
                Semih_Network ___network,
                Network_Host_Entities ___hostEntities)
            {
                bool flag = entity.entityType == EntityType.Player && damageInflictorEntityType == EntityType.Player;
                float num = damage;
                if (damageInflictorEntityType == EntityType.Player)
                {
                    num *= _currentWeaponDamage;
                }

                if (!GameManager.FriendlyFire && flag)
                {
                    return false;
                }

                if (flag)
                {
                    if (hitTransform != null)
                    {
                        if (hitTransform.tag == "Entity_Head")
                        {
                            num *= 3f;
                        }
                        else if (hitTransform.tag == "Entity_Chest")
                        {
                            num *= 2f;
                        }
                    }
                }

                if (entity.entityType == EntityType.Enemy && GameModeValueManager.GetCurrentGameModeValue()
                    .playerSpecificVariables.negateOutgoingPlayerDamage)
                {
                    num = 0f;
                }

                if (entity.entityType == EntityType.Player && damageInflictorEntityType == EntityType.Enemy)
                {
                    num *= GameModeValueManager.GetCurrentGameModeValue().playerSpecificVariables.damageTakenMultiplier;
                }
                else if (entity.entityType == EntityType.Enemy && damageInflictorEntityType == EntityType.Player)
                {
                    num *= GameModeValueManager.GetCurrentGameModeValue().playerSpecificVariables
                        .outgoingDamageMultiplierPVE;
                }

                if (___hostEntities == null)
                {
                    ___hostEntities = ComponentManager<Network_Host_Entities>.Value;
                }

                Message_NetworkEntity_Damage message = new Message_NetworkEntity_Damage(Messages.DamageEntity,
                    ___network.NetworkIDManager, ___hostEntities.ObjectIndex, entity.ObjectIndex, num, hitPoint,
                    hitNormal, damageInflictorEntityType, buffAsset);
                if (Semih_Network.IsHost)
                {
                    entity.Damage(num, hitPoint, hitNormal, damageInflictorEntityType, buffAsset);
                    if (num >= 0f)
                    {
                        ___network.RPC(message, Target.Other, EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
                        return false;
                    }
                }
                else if (num >= 0f)
                {
                    ___network.SendP2P(___network.HostID, message, EP2PSend.k_EP2PSendReliable,
                        NetworkChannel.Channel_Game);
                }

                return false;
            }
        }
        // ReSharper restore All
    }
}